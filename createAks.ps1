$azlogin=Get-Credential -message ("Enter your Endava email and password")
if ($azlogin -notlike "*@endava.com") {$userName=$azlogin.username + "@endava.com"} else {$userName=$azlogin.username}
if (az login -u $userName -p $azlogin.getnetworkcredential().password){
    write-output "Logon successfully, executing commands..."
    $userName=$userName.substring(0,$userName.indexof('@'))
    $resourceGroup="aksWorkShop"
    $location="eastus2"
    $vNetTemplate="vNet.json"
    $aksTemplate="aks.json"
    $aksName="akstest"
    $servicePrincipal="aks-" + $userName

    az group create --name $resourceGroup --location $location
    $appid=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query appId)
    $password=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query password)
    az group deployment create --template-file $vNetTemplate --resource-group $resourceGroup --verbose
    az group deployment create --template-file $aksTemplate --resource-group $resourceGroup --parameters `
                                        spClientId=$appid `
                                        spSecret=$password `
                                        --verbose
    az aks Get-Credentials --resource-group $resourceGroup --name $aksName --overwrite-existing
    kubectl config set-context $(kubectl config current-context)
    write-output "AKS cluster created successfully!"    
} else {
    write-output "Wrong credentials, review them!"
}