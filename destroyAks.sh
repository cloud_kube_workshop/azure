#!/bin/bash
read -p "Enter your Endava email: " username
read -s -p "Enter your password: " password
if [[ "$username" != *"@endava.com"*  ]]; then 
    username="$username@endava.com" 
fi
if [[ $(az login -u $username -p $password) ]]; then
    resourceGroup="aksWorkShop"
    echo "Logon successfully, removing resource group $resourceGroup..."
    username=${username%"@endava.com"}
    servicePrincipal="aks-$username"
    az group delete --name $resourceGroup --yes --verbose
    appid=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query appId --verbose)
    appid=${appid#\"} && appid=${appid%\"}
    az ad sp delete --id $appid --verbose
    echo "AKS Cluster deleted successfully!"
else
    echo "Wrong credentials, review them!"
fi